﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Pencil.Gaming;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;


namespace BatGame
{
    class MainClass
    {
        public static GlfwWindowPtr GlfwWindow;

        public static void Main(string[] args)
        {
            while (true)
                foreach (var line in GetLines())
                    Execute(line);
        }

        public static IEnumerable<string> GetLines()
        {
            while (true)
            {
                var lines = new string[0];
                try
                {
                    lines = File.ReadAllLines("BatGL");
                    File.Delete("BatGL");
                }
                catch
                {
                }

                if (lines.Length > 0)
                    foreach (var line in lines)
                        yield return line;
                Thread.Sleep(50);
            }
        }

        static void Execute(string line)
        {
            string command = line.Split(',')[0].Trim();
            string arguments_string = line.Contains(",") ? line.Substring(line.IndexOf(',') + 1) : "";
            string[] arguments = arguments_string.Split(',');

            switch (command.ToLower())
            {
                case "init": // init,Title,Width,Height
                    // Init main window and context
                    Glfw.Init();

                    string title = arguments.Length > 0 ? arguments[0] : "Game Window";
                    int Width = arguments.Length > 1 ? int.Parse(arguments[1]) : 800;
                    int Height = arguments.Length > 2 ? int.Parse(arguments[2]) : 600;

                    GlfwWindow = Glfw.CreateWindow(Width, Height, title, new GlfwMonitorPtr(), new GlfwWindowPtr());
                    Glfw.ShowWindow(GlfwWindow);
                    Glfw.MakeContextCurrent(GlfwWindow);

                    GL.Ortho(0, Width, 0, Height, 0f, 1f);


                    Glfw.SetWindowSizeCallback(GlfwWindow, (wnd, width, height) =>
                    {
                        GL.Viewport(0, 0, width, height);

                        GL.MatrixMode(MatrixMode.Projection);
                        GL.LoadIdentity();
                        GL.Ortho(0, 1f, 0, 1f, 0f, 1f);
                        GL.MatrixMode(MatrixMode.Modelview);
                    });
                    break;
                case "begin": // begin,(Any GL begin)
                    GL.Begin((BeginMode)Enum.Parse(typeof(BeginMode), arguments_string));
                    break;
                case "end": // end
                    GL.End();
                    break;
                case "vertex": // vertex,x,y(,z)
                    float x = float.Parse(arguments[0]);
                    float y = float.Parse(arguments[1]);

                    if (arguments.Length > 2)
                    {
                        float z = float.Parse(arguments[2]);
                        GL.Vertex3(x, y, z);
                    }
                    else
                        GL.Vertex2(x, y);

                    break;
                case "color": // color,r,g,b(,a) in 8-bit format
                    int r = int.Parse(arguments[0]);
                    int g = int.Parse(arguments[1]);
                    int b = int.Parse(arguments[2]);

                    if (arguments.Length > 3)
                    {
                        int a = int.Parse(arguments[3]);
                        GL.Color4((byte)r, (byte)g, (byte)b, (byte)a);
                    }
                    else
                        GL.Color3((byte)r, (byte)g, (byte)b);
                    break;
                case "line": // line,p1x,p1y,p2x,p2y,p1col,p2col
                    var p1 = new Vector2(
                        float.Parse(arguments[0]),
                        float.Parse(arguments[1])
                    );
                    var p2 = new Vector2(
                        float.Parse(arguments[2]),
                        float.Parse(arguments[3])
                    );

                    int r1 = int.Parse(arguments[4]);
                    int g1 = int.Parse(arguments[5]);
                    int b1 = int.Parse(arguments[6]);

                    int r2 = int.Parse(arguments[7]);
                    int g2 = int.Parse(arguments[8]);
                    int b2 = int.Parse(arguments[9]);

                    GL.Begin(BeginMode.Lines);
                    {
                        GL.Color3(r1, g1, b1);
                        GL.Vertex2(p1.X, p1.Y);
                        GL.Color3(r2, g2, b2);
                        GL.Vertex2(p2.X, p2.Y);
                    }
                    GL.End();
                    break;
                case "clear": // clear(,r,g,b(,a))
                    int rc, gc, bc, ac = 255;
                    rc = gc = bc = 0;

                    if (arguments.Length > 2)
                    {
                        rc = int.Parse(arguments[0]);
                        gc = int.Parse(arguments[1]);
                        bc = int.Parse(arguments[2]);

                        if (arguments.Length > 3)
                            ac = int.Parse(arguments[3]);
                    }
                    GL.ClearColor(new Color4((byte)rc, (byte)gc, (byte)bc, (byte)ac));
                    GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                    break;
                case "update": // update
                    Glfw.SwapBuffers(GlfwWindow);
                    Glfw.PollEvents();
                    break;
                case "set_fullscreen": // set_fullscreen(,width,heigth)
                    GlfwVidMode mode = Glfw.GetVideoMode(Glfw.GetPrimaryMonitor());
                    Width = mode.Width;
                    Height = mode.Height;

                    if (arguments.Length > 1)
                    {
                        Width = int.Parse(arguments[0]);
                        Height = int.Parse(arguments[1]);
                    }

                    Glfw.DestroyWindow(GlfwWindow);
                    GlfwWindow = Glfw.CreateWindow(Width, Height, "", Glfw.GetPrimaryMonitor(), new GlfwWindowPtr());
                    Glfw.MakeContextCurrent(GlfwWindow);
                    break;
                case "exit":
                    Glfw.DestroyWindow(GlfwWindow);
                    Environment.Exit(0);
                    break;
            }
        }
    }
}